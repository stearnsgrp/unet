# -*- coding: utf-8 -*-
"""
Mosaicking the Planet satellite images
@author: Siddharth Shankar
"""
from os import listdir
from os.path import isfile, join
import rasterio
from rasterio.merge import merge

#Paths
path = r'C:\Research\UNet\planet_order_361632/'
op = path+"planet_mosaic.tif" #output path with filename

#Read and sort *.tif files 
fileList = [file_ for file_ in listdir(path) if file_.endswith('.tif') if isfile(join(path, file_))]

sort = sorted(fileList, key=lambda planetImg: str(planetImg[planetImg.index('_')+1:].replace('.tif', ''))[0:2])

string_file = []
for files_ in sort:
    pathname = path+(files_)
    string_file.append(pathname)

#read objects added to the list
readobj = []
for files_ in string_file:
    src = rasterio.open(files_)
    readobj.append(src)
    
#Mosaic the rasters and update the metadata file
mosaic, out_trans = merge(readobj)

out_meta = src.meta.copy()
out_meta.update({"driver": "GTiff",
                 "height": mosaic.shape[1],
                  "width": mosaic.shape[2],
                  "transform": out_trans,
                  "crs": "EPSG:32624"
                  }
                )

with rasterio.open(op,"w",**out_meta) as dst:
    dst.write(mosaic)

